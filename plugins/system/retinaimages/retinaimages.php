<?php
/**
 *  @package    Retina Images
 *  @author     Josef Břehovský / EasyJoomla.org {@link http://www.easysjoomla.org}
 *  @copyright  2014 Josef Břehovský / EasyJoomla.org
 *  @authEmail  josef.brehovsky@easyjoomla.org
 *  @version    1.3
 *  @link       http://www.easyjoomla.org/less-compiler Documentation of LESS Compiler plugin
 *  @license    http://www.gnu.org/licenses/gpl-3.0.html GNU GPL 3.0
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
defined('_JEXEC') or die('Restricted access');
if(!defined('DS')) define('DS', DIRECTORY_SEPARATOR);

class PlgSystemRetinaImages extends JPlugin {
	public static $NAME = 'retinaimages';
	private $logInitialized = false;

	function __construct(&$subject, $config) {
		$app = JFactory::getApplication();
		if ($app->isAdmin()) return;

		parent::__construct($subject, $config);
	}

	public function onAfterRender() {

		if ($this->pixelRatioCheck() && $this->debugCheck()) {
			return;
		}

		$patterns = array(
			'@<img(\s+([a-z\-]{1,})=(["\']([^"\']*)["\']|[\S]))?(\s+([a-z\-]{1,})=(["\']([^"\']*)["\']|[\S]))?(\s+([a-z\-]{1,})=(["\']([^"\']*)["\']|[\S]))?(\s+([a-z\-]{1,})=(["\']([^"\']*)["\']|[\S]))?(\s+([a-z\-]{1,})=(["\']([^"\']*)["\']|[\S]))?(\s+([a-z\-]{1,})=(["\']([^"\']*)["\']|[\S]))?(\s+([a-z\-]{1,})=(["\']([^"\']*)["\']|[\S]))?(\s+([a-z\-]{1,})=(["\']([^"\']*)["\']|[\S]))?(\s+([a-z\-]{1,})=(["\']([^"\']*)["\']|[\S]))?(\s+([a-z\-]{1,})=(["\']([^"\']*)["\']|[\S]))?\s*/?>@isU',
		);

		$body = JResponse::getBody();
		$body = preg_replace_callback(
			$patterns,
			array($this, 'addSuffixCallback'),
			$body
		);

		JResponse::setBody($body);
	}

	public function pixelRatioCheck() {

		$return = true;
		$pixelRatio = 1;
		$ratioDetectorMethod = $this->params->get('ratio_detect', 'javascript-cookie');
		$base = dirname(__FILE__);

		switch ($ratioDetectorMethod) {
			case "javascript-cookie":
				JLoader::import('detector.javascript-cookie', $base);
				break;
		}

		$pixelRatioDetector = new pixelRatioDetector;
		$pixelRatio = $pixelRatioDetector->detect();

		if ($pixelRatio > 1) {
			$return = false;
		}

		return $return;
	}

	private function addSuffixCallback($matches) {

		$HDsrcAttribute = $this->params->get('hdsrc_attribute', 'data-hdsrc');

		$tag = $matches[0];
		$src = '';
		$HDsrc = '';
		$width = '';
		$height = '';

		for($i = 2; $i <= 40; $i = $i+4) {
			if ($matches[$i] == 'src') {
				$src = $matches[$i+2];
			}
			if ($matches[$i] == $HDsrcAttribute) {
				$HDsrc = $matches[$i+2];
			}
			if ($matches[$i] == 'width') {
				$width = $matches[$i+2];
			}
			if ($matches[$i] == 'height') {
				$height = $matches[$i+2];
			}
		}

		$path = $this->findPath($src);

		if ($path !== null) {
			$pathSuffix = $HDsrc != '' ? $HDsrc : $this->replacePath($path);
			$pathSuffix = JURI::base(true).str_replace(JURI::base(true), "", $pathSuffix);

			if ($pathSuffix !== null) {

				if ( $width == '' || $height == '') {
					$imageSize = $this->imageSize($path, $width, $height);
					list($imageWidth, $imageHeight) = $imageSize;
				}

				$replace = array(
					'patterns' => array(
						'@([\'"])' . $src . '([\'"])@i'
					),
					'replacements' => array(
						'$1' . $pathSuffix . '$2'
					)
				);
				if ( $width == '' && $imageWidth) {
					$replace['patterns'][] = '@(/?>)@i';
					$replace['replacements'][] = 'width="' . $imageWidth . '" $1';
				}
				if ( $height == '' && $imageHeight) {
					$replace['patterns'][] = '@(/?>)@i';
					$replace['replacements'][] = 'height="' . $imageHeight . '" $1';
				}

				return preg_replace($replace['patterns'], $replace['replacements'], $tag);
			}
		}

		return $tag;
	}

	private function findPath($uri) {
		$uri = trim($uri);
		$path = null;

		if (strpos($uri, 'http') === 0) {
			// http link
			$path = DS . str_replace(JURI::base(), '', $uri);
		} elseif ($uri[0] === '/') {
			// absolute URL
			$path = str_replace(JURI::base(true), '', $uri);
		} else {
			// relative URL
			$path = "/" . $uri;
		}

		jimport('joomla.filesystem.file');

		if ( ! JFile::exists(JPATH_ROOT . $path)) {
			$this->log(
				"Correct path for '$uri' not found (tried '" . JPATH_ROOT . "$path')",
				JLog::ERROR
			);

			return null;
		}

		return $path;
	}

	private function replacePath($path) {

		$suffix = $this->params->get('image_suffix', '@2x');

		$parts = explode(".", $path);
		$partsReversed = array_reverse($parts);
		$partsReversed[1] = $partsReversed[1].$suffix;
		$parts = array_reverse($partsReversed);
		$pathSuffix = implode(".", $parts);

		jimport('joomla.filesystem.file');

		$pathSuffixFull = JPATH_ROOT . $pathSuffix;

		if (!JFile::exists($pathSuffixFull)) {
			return $path;
		}

		return $pathSuffix;
	}

	private function imageSize($path, $width='', $height='') {

		$imageSize = array();

		$pathFull = JPATH_ROOT . $path;

		list($imageWidth, $imageHeight, $imageType, $imageAttr) = getimagesize($pathFull);

		$imageSize[0] = $width ? $width : $imageWidth;
		$imageSize[1] = $height? $height : $imageHeight;

		return $imageSize;
	}

	private function debugCheck() {

		$return = true;
		$debugEnabled = $this->params->get('enable_debug', 0);
		$retinaimagesForce = JRequest::getVar('force_retinaimages', 0);

		if($debugEnabled && $retinaimagesForce) {
			$return = false;
		}

		return $return;
	}

	private function log($message, $level) {
		if ( ! $this->logInitialized) {
			jimport('joomla.log.log');

			JLog::addLogger(
				array(
					'text_file' => 'retinaimages.php',
				),
				JLog::ALL,
				self::$NAME
			);

			$this->logInitialized = true;
		}

		JLog::add($message, $level, self::$NAME);
	}
}
