# README #

### What is this repository for? ###

* GBC site
* Version 2.1

#### Project Upload Status ####
[![buddy pipeline](https://app.buddy.works/buluma-1/farm-africa/pipelines/pipeline/62850/badge.svg?token=287807e6acbd363877fd8d9769eb71e57cb3fa02d4a93985fbf4730386050f36 "buddy pipeline")](https://app.buddy.works/buluma-1/farm-africa/pipelines/pipeline/62850)

#### Appveyor Project Status ####
[![Build status](https://ci.appveyor.com/api/projects/status/i4m8ik1e5evdwa2u?svg=true)](https://ci.appveyor.com/project/buluma/gbc-site)


### How do I get set up? ###

* Clone Repo
* Work on local branch
* export full database with drop table option selected
* upload db in db_dump as filename.sql (you may want to delete the previous filename.sql file)
* sync repo with bitbucket
* confirm site is working correctly: http://gbc.co.ke/

-------------------------------------------------------------------------
					DONT'S
-------------------------------------------------------------------------

1. Do not commit your configuration.php

### Who do I talk to? ###

* Michael Buluma